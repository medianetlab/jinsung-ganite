## Jobs data import (Jinsung Yoon, 10/11/2017)

import numpy as np
from scipy.special import expit

'''
Input: train_rate: 0.8
Outputs:
  - Train_X, Test_X: Train and Test features
  - Train_Y: Observable outcomes
  - Train_T: Assigned treatment
  - Opt_Train_Y, Test_Y: Potential outcomes.
'''

def Data_Twins(train_rate = 0.8):
  
    #%% Data Input (11400 patients, 30 features, 2 potential outcomes)
    Data = np.loadtxt("/home/vdslab/Documents/Jinsung/GANITE/Dataset/Final/Twin_Data.csv", delimiter=",",skiprows=1)

    # Features
    X = Data[:,:30]

    # Feature dimensions and patient numbers
    Dim = len(X[0])
    No = len(X)
    
    # Labels
    Opt_Y = Data[:,30:]
    Opt_Y = np.array(Opt_Y<9999,dtype=float)  # Die within 1 year = 1, otherwise = 0   
            
    #%% Patient Treatment Assignment
    coef = np.random.uniform(-0.01, 0.01, size = [Dim,1])
    Temp =  expit(np.matmul(X,coef) + np.random.normal(0,0.01, size = [No,1]) )
    
    Temp = Temp/(2*np.mean(Temp))
    
    Temp[Temp>1] = 1
    
    T = np.random.binomial(1,Temp,[No,1])
    T = T.reshape([No,])

    #%% Observable outcomes
    Y = np.zeros([No,1])

    # Output
    Y = np.transpose(T) * Opt_Y[:,1] + np.transpose(1-T) * Opt_Y[:,0]    
    Y = np.transpose(Y)
    Y = np.reshape(Y,[No,])

    #%% Train / Test Division
    temp = np.random.permutation(No)
    Train_No = int(train_rate*No)
    train_idx = temp[:Train_No]
    test_idx = temp[Train_No:No]
    
    Train_X = X[train_idx,:]
    Train_T = T[train_idx]
    Train_Y = Y[train_idx]
    Opt_Train_Y = Opt_Y[train_idx,:]

    Test_X = X[test_idx,:]
    Test_Y = Opt_Y[test_idx,:]
        
    return [Train_X, Train_T, Train_Y, Opt_Train_Y, Test_X, Test_Y]
  