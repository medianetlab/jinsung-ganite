'''
GANITE: 
Jinsung Yoon 10/11/2017
'''
# Necessary packages
import tensorflow as tf
import numpy as np
from tqdm import tqdm

#%% Import functions
import sys
sys.path.append('/home/vdslab/Documents/Jinsung/GANITE/GANITE_GitHub')
from Data_Twins import Data_Twins

#%% Data Input
train_rate = 0.8
[Train_X, Train_T, Train_Y, Opt_Train_Y, Test_X, Test_Y] = Data_Twins(train_rate)

#%% GANITE
#%% System Parameters
# 1. Mini batch size
mb_size = 128
# 2. System Parameters
alpha = 0.1
Rand_Dim = 2

Train_No = len(Train_X)
Test_No = len(Test_X)

Dim = len(Train_X[0])
H_Dim1 = int(Dim/2)
H_Dim2 = int(Dim/2)

#%% Necessary functions
# Xavier Initialization Definition
def xavier_init(size):
    in_dim = size[0]
    xavier_stddev = 1. / tf.sqrt(in_dim / 2.)
    return tf.random_normal(shape = size, stddev = xavier_stddev)

#%% 1. Input
# 1.1. Feature (X)
X = tf.placeholder(tf.float32, shape = [None, Dim])
# 1.2. Treatment (T)
T = tf.placeholder(tf.float32, shape = [None, 1])
# 1.3. Outcome (Y)
Y = tf.placeholder(tf.float32, shape = [None, 1])
# 1.4. Random Noise (G)
Z_G = tf.placeholder(tf.float32, shape = [None, Rand_Dim])
# 1.5. Random Noise (I)
Z_I = tf.placeholder(tf.float32, shape = [None, Rand_Dim])
# 1.6. Test Outcome (Y_T) - Potential outcome
Y_T = tf.placeholder(tf.float32, shape = [None, 2])

#%% 2. layer construction
# 2.1 Generator Layer
G_W1 = tf.Variable(xavier_init([(Dim+2+Rand_Dim), H_Dim1]))   # Inputs: X + Treatment (1) + Factual Outcome (1) + Random Vector (Z)
G_b1 = tf.Variable(tf.zeros(shape = [H_Dim1]))

G_W2 = tf.Variable(xavier_init([H_Dim1, H_Dim2]))
G_b2 = tf.Variable(tf.zeros(shape = [H_Dim2]))

G_W3 = tf.Variable(xavier_init([H_Dim2, 2]))
G_b3 = tf.Variable(tf.zeros(shape = [2]))   # Output: Estimated Potential Outcomes

theta_G = [G_W1, G_W2, G_W3, G_b1, G_b2, G_b3]

# 2.2 Discriminator
D_W1 = tf.Variable(xavier_init([(Dim+2), H_Dim1]))   # Inputs: X + Factual Outcomes + Estimated Counterfactual Outcomes
D_b1 = tf.Variable(tf.zeros(shape = [H_Dim1]))

D_W2 = tf.Variable(xavier_init([H_Dim1, H_Dim2]))
D_b2 = tf.Variable(tf.zeros(shape = [H_Dim2]))

D_W3 = tf.Variable(xavier_init([H_Dim2, 1]))
D_b3 = tf.Variable(tf.zeros(shape = [1]))

theta_D = [D_W1, D_W2, D_W3, D_b1, D_b2, D_b3]

# 2.3 Inference Layer
I_W1 = tf.Variable(xavier_init([(Dim+Rand_Dim), H_Dim1]))
I_b1 = tf.Variable(tf.zeros(shape = [H_Dim1]))

I_W2 = tf.Variable(xavier_init([H_Dim1, H_Dim2]))
I_b2 = tf.Variable(tf.zeros(shape = [H_Dim2]))

I_W3 = tf.Variable(xavier_init([H_Dim2, 2]))
I_b3 = tf.Variable(tf.zeros(shape = [2]))

theta_I = [I_W1, I_W2, I_W3, I_b1, I_b2, I_b3]

# 2.4 Inference Discriminator
ID_W1 = tf.Variable(xavier_init([(Dim+2), H_Dim1]))   # Inputs: X + Estimated Potential Outcomes (Or outputs of Counterfactual block)
ID_b1 = tf.Variable(tf.zeros(shape = [H_Dim1]))

ID_W2 = tf.Variable(xavier_init([H_Dim1, H_Dim2]))
ID_b2 = tf.Variable(tf.zeros(shape = [H_Dim2]))

ID_W3 = tf.Variable(xavier_init([H_Dim2, 1]))
ID_b3 = tf.Variable(tf.zeros(shape = [1]))

theta_ID = [ID_W1, ID_W2, ID_W3, ID_b1, ID_b2, ID_b3]

#%% 3. Definitions of multiple functions

# 3.1 Generator
def generator(x,t,y,z):
    inputs = tf.concat(axis = 1, values = [x,t,y,z])
    G_h1 = tf.nn.relu(tf.matmul(inputs, G_W1) + G_b1)
    G_h2 = tf.nn.relu(tf.matmul(G_h1, G_W2) + G_b2)
    G_prob = tf.nn.sigmoid(tf.matmul(G_h2, G_W3) + G_b3)
    
    return G_prob
    
# 3.2. Discriminator
def discriminator(x,t,y,hat_y):
    # Factual & Counterfactual outcomes concatenate
    inp0 = (1.-t) * y + t * tf.reshape(hat_y[:,0], [-1,1])
    inp1 = t * y + (1.-t) * tf.reshape(hat_y[:,1], [-1,1])
    
    inputs = tf.concat(axis = 1, values = [x,inp0,inp1])
    
    D_h1 = tf.nn.relu(tf.matmul(inputs, D_W1) + D_b1)
    D_h2 = tf.nn.relu(tf.matmul(D_h1, D_W2) + D_b2)
    D_logit = tf.matmul(D_h2, D_W3) + D_b3
    D_prob = tf.nn.sigmoid(D_logit)
    
    return D_prob

# 3.3. Inference Nets
def inference(x,r):
    inputs = tf.concat(axis = 1, values = [x,r])
    I_h1 = tf.nn.relu(tf.matmul(inputs, I_W1) + I_b1)
    I_h2 = tf.nn.relu(tf.matmul(I_h1, I_W2) + I_b2)
    I_prob = tf.nn.sigmoid(tf.matmul(I_h2, I_W3) + I_b3)
    
    return I_prob
  
# 3.4. Inference Discriminator
def I_discriminator(x,hat_y):
    
    inputs = tf.concat(axis = 1, values = [x,hat_y])
    ID_h1 = tf.nn.relu(tf.matmul(inputs, ID_W1) + ID_b1)
    ID_h2 = tf.nn.relu(tf.matmul(ID_h1, ID_W2) + ID_b2)
    ID_logit = tf.matmul(ID_h2, ID_W3) + ID_b3
    ID_prob = tf.nn.sigmoid(ID_logit)
    
    return ID_prob

# 3.5 Others
# Random sample generator for Z and R
def sample_Z(m, n):
    return np.random.uniform(-1., 1., size = [m, n])        

# Mini-batch generation
def sample_X(X, size):
    start_idx = np.random.randint(0, X.shape[0], size)
    return start_idx

#%% Structure
# 1. Generator
Tilde = generator(X,T,Y,Z_G)
# 2. Discriminator
D_prob = discriminator(X,T,Y,Tilde)
# 3. Inference function
Hat = inference(X,Z_I)
# 4. Inference discriminator
ID_prob_real = I_discriminator(X, Tilde)
ID_prob_fake = I_discriminator(X, Hat)

#%% Loss
# 1. Discriminator loss
D_loss = -tf.reduce_mean(T * tf.log(D_prob + 1e-8) + (1. -T) * tf.log(1. - D_prob + 1e-8)) 

# 2. Generator loss
G_loss_GAN = -D_loss 

G_loss_R = tf.reduce_mean( (Y - (T * Tilde[:,1] + (1. -T) * Tilde[:,0]) )**2 ) 

G_loss = tf.sqrt(G_loss_R) + alpha * G_loss_GAN

# 3. Inference discriminator loss
ID_loss = -tf.reduce_mean(tf.log(ID_prob_real + 1e-8) + tf.log(1. - ID_prob_fake + 1e-8))

# 4. Inference loss
I_loss1 = tf.reduce_mean( ( ( (1. - T) * Hat[:,0] - (1. - T) * Y ) + ( T * Hat[:,1] - T * Y)  ) **2) 
I_loss2 = tf.reduce_mean( ( ( (1. - T) * Hat[:,1] - (1. - T) * Tilde[:,1]) + (T * Hat[:,0] - T * Tilde[:,0])  ) **2) 
I_loss = tf.sqrt(I_loss1) + tf.sqrt(I_loss2) - alpha * ID_loss

#%% Performance Metrics
def PEHE(y, hat_y):
    e_PEHE = tf.reduce_mean(( (y[:,1]-y[:,0]) - (hat_y[:,1] - hat_y[:,0]) )**2)
    return e_PEHE

def ATE(y, hat_y):
    e_PEHE = tf.abs( tf.reduce_mean( y[:,1]-y[:,0] ) - tf.reduce_mean( hat_y[:,1]-hat_y[:,0] ) )
    return e_PEHE

# Loss Followup    
Hat_Y = Hat

Loss1 = PEHE(Y_T,Hat_Y)
Loss2 = ATE(Y_T,Hat_Y)

#%% Solver
G_solver = tf.train.AdamOptimizer().minimize(G_loss, var_list=theta_G)
D_solver = tf.train.AdamOptimizer().minimize(D_loss, var_list=theta_D)
I_solver = tf.train.AdamOptimizer().minimize(I_loss, var_list=theta_I)
ID_solver = tf.train.AdamOptimizer().minimize(ID_loss, var_list=theta_ID)

#%% Sessions
sess = tf.Session()
sess.run(tf.global_variables_initializer())
    
# Iterations
# Train G and D first
for it in tqdm(range(5000)):
  
    idx_mb = sample_X(Train_X, mb_size) 
    X_mb = Train_X[idx_mb,:]
    T_mb = np.reshape(Train_T[idx_mb], [mb_size,1]) 
    Y_mb = np.reshape(Train_Y[idx_mb], [mb_size,1])   
    Z_G_mb = sample_Z(mb_size, Rand_Dim)
    
    _, D_loss_curr = sess.run([D_solver, D_loss], feed_dict = {X: X_mb, T: T_mb, Y: Y_mb, Z_G: Z_G_mb})
    
    idx_mb = sample_X(Train_X, mb_size) 
    X_mb = Train_X[idx_mb,:]
    T_mb = np.reshape(Train_T[idx_mb], [mb_size,1]) 
    Y_mb = np.reshape(Train_Y[idx_mb], [mb_size,1])   
    Z_G_mb = sample_Z(mb_size, Rand_Dim)
    
    _, G_loss_curr = sess.run([G_solver, G_loss], feed_dict = {X: X_mb, T: T_mb, Y: Y_mb, Z_G: Z_G_mb})
        
    #%% Testing
    if it % 100 == 0:
        print('Iter: {}'.format(it))
        print('D_loss: {:.4}'.format((D_loss_curr)))        
        print('G_loss: {:.4}'.format((G_loss_curr)))    
        print()
    
# Train I and ID
for it in tqdm(range(5000)):
  
    idx_mb = sample_X(Train_X, mb_size) 
    X_mb = Train_X[idx_mb,:]
    T_mb = np.reshape(Train_T[idx_mb], [mb_size,1]) 
    Y_mb = np.reshape(Train_Y[idx_mb], [mb_size,1])   
    Z_G_mb = sample_Z(mb_size, Rand_Dim)
    Z_I_mb = sample_Z(mb_size, Rand_Dim)
    
    _, ID_loss_curr = sess.run([ID_solver, ID_loss], feed_dict = {X: X_mb, T: T_mb, Y: Y_mb, Z_G: Z_G_mb, Z_I: Z_I_mb})
    
    idx_mb = sample_X(Train_X, mb_size) 
    X_mb = Train_X[idx_mb,:]
    T_mb = np.reshape(Train_T[idx_mb], [mb_size,1]) 
    Y_mb = np.reshape(Train_Y[idx_mb], [mb_size,1])   
    Z_G_mb = sample_Z(mb_size, Rand_Dim)
    
    _, I_loss_curr = sess.run([I_solver, I_loss], feed_dict = {X: X_mb, T: T_mb, Y: Y_mb, Z_G: Z_G_mb, Z_I: Z_I_mb})    

    #%% Testing
    if it % 100 == 0:
        New_X_mb = Test_X 
        Y_T_mb = Test_Y           
        Z_G_mb = sample_Z(Test_No, Rand_Dim)
        Z_I_mb = sample_Z(Test_No, Rand_Dim)
    
        Loss1_curr, Loss2_curr, Hat_curr = sess.run([Loss1, Loss2, Hat], feed_dict = {X: New_X_mb, Y_T: Y_T_mb, Z_G: Z_G_mb, Z_I: Z_I_mb})        
        
        print('Iter: {}'.format(it))
        print('ID_loss: {:.4}'.format((ID_loss_curr)))   
        print('I_loss: {:.4}'.format((I_loss_curr)))      
        print('Loss_PEHE_Out: {:.4}'.format(np.sqrt(Loss1_curr)))   
        print('Loss_ATE_Out: {:.4}'.format(Loss2_curr))   
        print()
        
        